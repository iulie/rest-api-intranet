<?php
use App\Controllers\Auth\AuthenticatorController;
use App\Controllers\TeamController;
use App\Controllers\EmployeeController;
use App\Controllers\ProjectController;


$route->get('/', function() {
    view('dashboard.index');
});

$route->get('/team',[TeamController::class,'index']);
$route->get('/team/create',[TeamController::class,'create']);
$route->post('/team',[TeamController::class,'store']);
$route->get('/team/edit',[TeamController::class,'edit']);
$route->put('/team',[TeamController::class,'update']);
$route->delete('/team',[TeamController::class,'destroy']);
$route->get('/team/show',[TeamController::class,'show']);
$route->get('/auth/login',[AuthenticatorController::class, 'index']);
$route->post('/auth/login',[AuthenticatorController::class, 'store']);
$route->post('/auth/logout',[AuthenticatorController::class, 'logout']);

$route->get('/employee',[EmployeeController::class,'index']);
$route->get('/employee/create',[EmployeeController::class,'create']);
$route->post('/employee',[EmployeeController::class,'store']);
$route->get('/employee/edit',[EmployeeController::class,'edit']);
$route->put('/employee',[EmployeeController::class,'update']);
$route->delete('/employee',[EmployeeController::class,'destroy']);

$route->get('/project',[ProjectController::class,'index']);
$route->get('/project/create',[ProjectController::class,'create']);
$route->post('/project',[ProjectController::class,'store']);
$route->get('/project/edit',[ProjectController::class,'edit']);
$route->put('/project',[ProjectController::class,'update']);
$route->delete('/project',[ProjectController::class,'destroy']);
$route->get('/project/show',[ProjectController::class,'show']);