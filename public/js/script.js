(function($){

 
    function convertForm2Json(form){
        const array = $(form).serializeArray();
        const json = {};
        $.each(array,function(){
            json[this.name] = this.value;
        });
        return JSON.stringify(json);
    }

    $('form.delete').on('submit',(e)=>{
        e.preventDefault();
        const form = $(e.target);
        $('#deleteModalLabel').text(form.data('title')); //test()  innerText
        $('#deleteModal .modal-body').html(form.data('body')); //html()  innerHTML
        
        $('#deleteModal .btn-danger').off().on('click',(e)=>{
            $.ajax({
                url: form.attr('action'),
                method: 'delete',
                contentType: 'application/json',
                data: convertForm2Json(form),
                success : function(response){
                    if( response.status == 200){
                        form.closest('tr').remove();
                    }
                },
                error: function(xhr){
                    console.log(xhr.status,xhr.statusText);
                }
            })
        })
    });
    
})(jQuery)
/*
"use strict"
document.querySelectorAll(".nav-item").forEach(classElement => {
    classElement.addEventListener('click', () => {
        console.log('click');
        classElement.querySelector('.nav-link').classList.toggle('active');
    })
});

const navbar = document.getElementById('navbarSupportedContent');

navbar.addEventListener('click', function(event) {
    console.log('click');
    if (event.target.classList.contains('nav-link')) {
        const links = navbar.getElementsByClassName('nav-link');
        for (let i = 0; i < links.length; i++) {
            links[i].classList.remove('active');
        }
        event.target.classList.add('active');
    }
});*/








