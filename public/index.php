<?php

use App\Bootstrap;

define('APP_ROOT_DIR', dirname(__DIR__) );

require_once("../bootstrap.php");

require_once APP_ROOT_DIR.'/vendor/autoload.php';
require_once APP_ROOT_DIR.'/helpers/helpers.php';

$app = Bootstrap::init();

$route = $app::$route;

require APP_ROOT_DIR.'/routes/web.php';

$app->run();