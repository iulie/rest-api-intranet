<?php

return <<<SQL
    CREATE TABLE IF NOT EXISTS employee_project(
        employee_id INT UNSIGNED NOT NULL,
        project_id INT UNSIGNED NOT NULL,
        PRIMARY KEY(employee_id,project_id),

        CONSTRAINT fk_employee_id FOREIGN KEY(employee_id) REFERENCES employees(id) ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE ON UPDATE CASCADE
    )ENGINE=INNODB;
SQL;