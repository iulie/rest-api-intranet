<?php

return <<<SQL
    CREATE TABLE IF NOT EXISTS employees(
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        team_id INT UNSIGNED,
        firstname VARCHAR(70) NOT NULL,
        lastname VARCHAR(100) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(125) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY(id),

        CONSTRAINT fk_department_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE RESTRICT ON UPDATE CASCADE
    )ENGINE=INNODB;
SQL;