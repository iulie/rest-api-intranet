<?php

return <<<SQL
    CREATE TABLE IF NOT EXISTS projects(
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        name VARCHAR(70) NOT NULL,
        description text NOT NULL,
        image VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY(id)
    )ENGINE=INNODB;
SQL;