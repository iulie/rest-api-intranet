<?php



define('APP_ROOT_DIR', dirname(__DIR__) );

require_once APP_ROOT_DIR.'/vendor/autoload.php';
require_once APP_ROOT_DIR.'/helpers/helpers.php';

/*
    1. Datenbankverbindung (App\Core\Mysql.php) aufbauen
    2. Tabelle migrations erzeugen
    3. Dateien in der die DB Tabellen definiert werden (database/migrations)
    3.1 Dateien eingelesen werden und das SQL Statement abgesetzt werden
    3.2 Dateiname in die Tabelle migrations eintragen

*/


$dbClass = '\App\Core\\'.ucfirst( config('database.mysql.driver') );
if( !class_exists($dbClass) ){
   dd( 'Die Klasse '.$dbClass.' existiert nicht');
}
$db = new $dbClass( config('database.mysql') );
if( $db->getError('con') ){
    dd( 'this error='.$db->getError('con') );
}

$sql = <<<SQL
    CREATE TABLE IF NOT EXISTS migrations(
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        migrate VARCHAR(255) NOT NULL,
        PRIMARY KEY(id)
    )ENGINE=INNODB;
SQL;

$db->query($sql);
if( $db->getError('query') ){
    dd( $db->getError('query') );
}
$db->table('migrations');

$pfad = APP_ROOT_DIR.'/database/migrations';
$migrationFiles = scandir($pfad);

$notMigrateFiles = ['.','..'];

$records = $db->all();
foreach( $records as $migrate ){
    $notMigrateFiles[] = $migrate->migrate;
}

$migrationFiles = array_diff($migrationFiles,$notMigrateFiles);

if( empty($migrationFiles) ){
    echo "nichts zu migrieren\r\n";
    exit;
}

foreach( $migrationFiles as $migrate){
    $sql = require $pfad.'/'.$migrate;
    $db->query($sql);
    if( $db->getError('query') ){
        dd($db->getError('query'));
    }
    if( !$db->create(['migrate'=>$migrate ]) ){
        dd($db->getError('create'));
    } 
    echo $migrate."\r\n";
}