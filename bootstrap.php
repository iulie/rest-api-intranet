<?php
function fatal_handler(): void
{
    $error = error_get_last();
    if($error){
        echo "Kritischer Fehler:\n";
        var_dump($error);
    }
}
register_shutdown_function( "fatal_handler" );