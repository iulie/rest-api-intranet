<?php

require_once("../app/Core/view.php");

use App\Core\View;

function view(string $template, array $data=[], string $templatePath=''): void
{
    $template = '/'.str_replace('.','/',$template).'.php';
    if( !$templatePath ){
        $templatePath = APP_ROOT_DIR.'/resources/views';
    }

    $view = new View($templatePath, $template, $data);
    echo $view->load();
}