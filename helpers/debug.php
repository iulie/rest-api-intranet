<?php

function dump($data){
    echo '<pre style="background:#333; color:#fff; padding:10px">';
    var_dump($data);
    echo '</pre>';
}

function dd($data){
    dump($data);
    exit;
}