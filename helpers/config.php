<?php

function config(string $data){

    // 1. String zerlegen anhand Trennzeichen .
    $data = explode('.',$data);
   
    //2. Erster Teilstring Dateiname im Ordner config
    $configFile = APP_ROOT_DIR.'/config/'.$data[0].'.php';

    // 2.1 Prüfen ob Datei existiert
        //Falls Nein => return '';
        if( !file_exists($configFile) ){
           
            return '';
        }
        unset( $data[0] ); //Pfad aus data Array entfernen

        //Falls Ja => Datei einbinden
        $config = require $configFile;
    
    //3. Schreife durchlaufen bis letzter Key gefunden
    foreach( $data as $key ){
        //Key überprüfen
            //Falls nein return ''
        if( !isset($config[$key])){
            return '';
        }
        $config = $config[$key];
    }
    //Inhalt zurückgeben (letzten Key)
   
    return $config;
}