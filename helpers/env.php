<?php

function env($key,$default=""){
    if( empty($_ENV) ){
        readEnv();
    }

    return $_ENV[$key] ?? $default;
}

function readEnv(){
    /*
        1. Prüfen ob .env Datei vorhanden ist
            Falls nein -> retrun false
            Falls ja -> Datei einlesen (fopen)
    */
    if( !file_exists(APP_ROOT_DIR.'/.env') || 
        ($handle = fopen(APP_ROOT_DIR.'/.env','r')) === false){
        return false;
    }

    // 2. inhalt von .env Zeilenweise einlesen (fgets)
    while( $line = fgets($handle,1024) ){
        $line = trim($line);
        /*
        3. Zeile prüfen:
            3.1 Leerzeilen ignorieren
            3.2 Zeilen die mit ein # beginnen ignorieren
            3.3 Zeile ohne = ignorieren
        */
        if( strpos($line,'#') === 0 ||
            empty($line) || 
            strpos($line,'=') === false ){
                continue;
        }

        //4. Zeile anhand dem = Zeichen zerlegen
        list($key,$value) = explode('=',$line,2);
        $key = trim($key);
        $value = trim($value);

        /*
        5. Teilstring vor dem = Zeichen -> Key von $_ENV
            Teilstring nach dem = Zeichen -> dazugehörende Value
            z.B. $_ENV['APP_NAME'] = "Wifi 051";
            Besonderheit
            Falls der Teilstring nach dem = Zeichen den String true oder false hat, dann soll dies in ein Boolan umgewandelt werden.
        */
        if( $value == 'true'){
            $_ENV[$key] = true;
        }
        else if( $value == 'false'){
            $_ENV[$key] = false;
        }
        else{
            $_ENV[$key] = trim($value,'"');
        }
    }
}