<?php
    include $this->path.'/layouts/header.php';
?>
    <form action="/auth/login" method="POST">
        <?php
        if (isset($_SESSION["error_login"])) {?>
            <div class="alert alert-danger" role="alert">
                <?php echo $_SESSION["error_login"]; ?>
            </div>
           <?php unset($_SESSION["error_login"]);
        }
        ?>
        <div class="w-50 p-3">
            <h3 class="mb-4">Login</h3>
            <div class="form-floating mb-3">
                <input type="email" class="form-control <?php if(isset($this->errors['email'])) echo 'is-invalid'; ?>" name="email" id="floatingInput" placeholder="name@example.com">
                <?php
                if(isset($this->errors['email'])){
                    echo ' <div class="invalid-feedback">'.$this->errors['email'].'</div>';
                }
                ?>
                <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control <?php if(isset($this->errors['password'])) echo 'is-invalid'; ?>" name="password" id="floatingPassword" placeholder="Password" >
                <?php
                if(isset($this->errors['password'])){
                    echo ' <div class="invalid-feedback">'.$this->errors['password'].'</div>';
                }
                ?>
                <label for="floatingPassword">Password</label>
            </div>
            <button type="submit" class="btn btn-primary mt-3">Login</button>
        </div>
    </form>
<?php
include $this->path.'/layouts/footer.php';
?>