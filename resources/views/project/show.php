<?php
  include $this->path.'/layouts/header.php';
?>
 <div id="wrapper" class="container-fluid">
        <h1><?= $this->project->name; ?></h1>
        <div class="button"><a href="/project" class="btn btn-outline-secondary">All Projects</a></div>
        <p><?= $this->project->description; ?></p>
        <img src="/uploads/<?= $this->project->image; ?>" alt="">
 </div>
<?php
  include $this->path.'/layouts/footer.php';
?>