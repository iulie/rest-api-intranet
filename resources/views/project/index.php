<?php
  include $this->path.'/layouts/header.php';
?>

<div id="wrapper" class="container-fluid">
        <h1>Projekte</h1>
        <div class="button"><a href="/project/create" class="btn btn-outline-secondary">Neues Projekt</a></div>

        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Bild</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
            <?php
                foreach($this->projects as $project) :
              ?>
              <tr>
                <th scope="row"><?= $project->id;?></th>
                <td><?= $project->name; ?></td>
                <td><img src="uploads/<?= $project->image; ?>" width="150" alt="Wifi Wien"></td>
                <td><a href="/project/show?id=<?= $project->id; ?>" class="btn btn-outline-dark fa fa-eye"></a></td>
                <td><a href="/project/edit?id=<?= $project->id; ?>" class="btn btn-outline-dark fa fa-edit"></a></td>
                <td scope="col"><form action="/project" method="post" class="delete" data-title="<?= $project->name; ?>" 
                    data-body="Should the project <strong><?= $project->name; ?></strong> be deleted?">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="<?= $project->id;?>">
                    <button type="submit" class="btn btn-outline-danger fa fa-trash" data-bs-toggle="modal" data-bs-target="#deleteModal"></button>
                </form></td>
              </tr>
              <?php
                endforeach;
              ?>
            </tbody>
          </table>
</div>
<?php
  include $this->path.'/layouts/footer.php';
?>