<?php
  include $this->path.'/layouts/header.php';
?>
   <div id="wrapper" class="container-fluid">
        <h1>Neues Projekt</h1>
        <div class="button"><a href="/project" class="btn btn-outline-secondary">Alle Projekte</a></div>

        <div id="form" class="form">
            <form action="/project" method="POST" enctype="multipart/form-data">
                <div class="form-group mb-2">
                  <label for="name">Name</label>
                  <input type="text" class="form-control <?php if(isset($this->errors['name'])) echo 'is-invalid'; ?>" 
                         name="name" id="name" value="<?= $this->data['title'] ?? ''; ?>" required>
                  <?php
                     if(isset($this->errors['name'])){
                       echo '<div class="invalid-feedback">'.$this->errors['name'].'</div>';
                     }
                  ?>
                </div>
                <div class="form-group mb-2">
                  <label for="description">Beschreibung</label>
                  <textarea class="form-control <?php if(isset($this->errors['description'])) echo 'is-invalid'; ?>" 
                            id="description" name="description" rows="3" value="<?= $this->data['body'] ?? ''; ?>" required>
                  </textarea>
                  <?php
                     if(isset($this->errors['description'])){
                       echo '<div class="invalid-feedback">'.$this->errors['description'].'</div>';
                     }
                  ?>
                </div>
                <div class="form-group mb-2">
                    <label for="image">Bild</label>
                    <input type="file" class="form-control" name="image" id="image" accept="image/png, image/jpeg, image/gif" required>
                </div>
                <button type="submit" class="btn btn-dark">speichern</button>
              </form>
        </div>
    </div>

<?php
  include $this->path.'/layouts/footer.php';
?>