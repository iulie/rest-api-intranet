<?php
include $this->path . '/layouts/header.php';
?>
<div id="wrapper" class="container-fluid">
  <h1><?= $this->data['name']; ?> ändern</h1>
  <div class="button"><a href="/project" class="btn btn-outline-secondary">Alle Projekte</a></div>

  <div id="form" class="form">
    <form action="/project" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="<?= $this->data['id'];?>">
      <div class="form-group mb-2">
        <label for="name">Name</label>
        <input type="text" class="form-control <?php if (isset($this->errors['name'])) echo 'is-invalid'; ?>" name="name" id="name" value="<?= $this->data['name'] ?? ''; ?>" required>
        <?php
        if (isset($this->errors['name'])) {
          echo '<div class="invalid-feedback">' . $this->errors['name'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="description">Beschreibung</label>
        <textarea class="form-control <?php if (isset($this->errors['description'])) echo 'is-invalid'; ?>" id="description" name="description" rows="3" value="<?= $this->data['description'] ?? ''; ?>" required>
                  </textarea>
        <?php
        if (isset($this->errors['description'])) {
          echo '<div class="invalid-feedback">' . $this->errors['description'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="image">Image</label>
        <input type="file" class="form-control" name="image" id="image" accept="image/png, image/jpeg, image/gif" value="uploads/<?= $this->data['image'] ?? ''; ?>" required>
      </div>
      <button type="submit" class="btn btn-dark">speichern</button>
    </form>
  </div>
</div>

<?php
include $this->path . '/layouts/footer.php';
?>