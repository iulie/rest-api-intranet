<?php
  include $this->path.'/layouts/header.php';
?>
 <div id="wrapper" class="container-fluid">
        <h1><?= $this->team->title; ?></h1>
        <div class="button"><a href="/team" class="btn btn-outline-secondary">All Teams</a></div>
        <p><?= $this->team->body; ?></p>
        <h2>Employees</h2>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Firstname</th>
              <th>Lastname</th>
              <th>E-Mail</th>
            </tr>
          </thead>
          <tbody>
              <?php
                foreach($this->employees as $employee) :
              ?>
            <tr>
              <td><?= $employee->id; ?></td>
              <td><?= $employee->firstname; ?></td>
              <td><?= $employee->lastname; ?></td>
              <td><?= $employee->email; ?></td>
            </tr>
              <?php
                endforeach;
              ?>
          </tbody>
        </table>
 </div>
        
<?php
  include $this->path.'/layouts/footer.php';
?>