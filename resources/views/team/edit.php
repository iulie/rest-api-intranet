<?php
  include $this->path.'/layouts/header.php';
?>
      <div id="wrapper" class="container-fluid">
        <h1><?= $this->data['title']; ?> ändern</h1>
        <div class="button"><a href="/team" class="btn btn-outline-secondary">All Teams</a></div>

        <div id="form" class="form">
            <form action="/team" method="POST">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="<?= $this->data['id'];?>">
                <div class="form-group mb-2">
                  <label for="title">Name</label>
                  <input type="text" class="form-control <?php if(isset($this->errors['title'])) echo 'is-invalid'; ?>" name="title" id="title" value="<?= $this->data['title'] ?? ''; ?>">
                  <?php
                     if(isset($this->errors['title'])){
                       echo '<div class="invalid-feedback">'.$this->errors['title'].'</div>';
                     }
                  ?>
                </div>
                <div class="form-group mb-2">
                  <label for="body">Beschreibung</label>
                  <textarea class="form-control <?php if(isset($this->errors['body'])) echo 'is-invalid'; ?>" id="body" name="body" rows="3"><?= $this->data['body'] ?? ''; ?></textarea>
                  <?php
                     if(isset($this->errors['body'])){
                       echo '<div class="invalid-feedback">'.$this->errors['body'].'</div>';
                     }
                  ?>
                </div>

                <button type="submit" class="btn btn-dark">ändern</button>
              </form>
        </div>
      </div>
<?php
  include $this->path.'/layouts/footer.php';
?>