<?php
  include $this->path.'/layouts/header.php';
?>
  <div id="wrapper" class="container-fluid">
        <h1>All Teams</h1>
        <div class="button"><a href="/team/create" class="btn btn-outline-secondary">New Team</a></div>
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($this->teams as $team) :
              ?>
              <tr>
                <th scope="col"><?= $team->id; ?></th>
                <td scope="col"><?= $team->title; ?></td>
                <td><a href="/team/show?id=<?= $team->id; ?>" class="btn btn-outline-dark fa fa-eye"></a></td>
                <td><a href="/team/edit?id=<?= $team->id; ?>" class="btn btn-outline-dark fa fa-edit"></a></td>
                <td scope="col"><form action="/team" method="post" class="delete" data-title="<?= $team->title; ?>" data-body="Should the team <strong><?= $team->title; ?></strong> be deleted?">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="<?= $team->id;?>">
                    <button type="submit" class="btn btn-outline-danger fa fa-trash" data-bs-toggle="modal" data-bs-target="#deleteModal"></button>
                </form></td>
              </tr>
              <?php
                endforeach;
              ?>
            </tbody>
          </table>
  </div>
<?php
  include $this->path.'/layouts/footer.php';
?>