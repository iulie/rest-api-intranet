<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Abteilung</title>
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="/">Intranet</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" 
          data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
          aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <div class="subnav">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/team">Team</a>
              </li>
            </div>
              <li class="nav-item ">
                <a class="nav-link " href="/employee">Employee</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/project">Project</a>
              </li>
                <li class="nav-item">
                    <form method="POST" action="/auth/logout">
                        <button type="submit" class="nav-link" style="background-color: transparent; all: unset; cursor: pointer;"><a class="nav-link">Logout</a></button>
                    </form>
                </li>
            </ul>
          </div>
        </div>
    </nav>
    <div id="wrapper" class="container-fluid">
    