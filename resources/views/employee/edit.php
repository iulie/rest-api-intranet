<?php
include $this->path . '/layouts/header.php';
?>
<div id="wrapper" class="container-fluid">
  <h1><?= $this->data['firstname']; ?> ändern</h1>
  <div class="button"><a href="/employee" class="btn btn-outline-secondary">Alle Mitarbeiter:innen</a></div>

  <div id="form" class="form">
    <form action="/employee" method="POST">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="<?= $this->data['id'];?>">
      <div class="form-group mb-2">
        <label for="firstname">Vorname</label>
        <input type="text" class="form-control <?php if (isset($this->errors['firstname'])) echo 'is-invalid'; ?>" name="firstname" id="firstname" value="<?= $this->data['firstname'] ?? ''; ?>" required>
        <?php
        if (isset($this->errors['firstname'])) {
          echo '<div class="invalid-feedback">' . $this->errors['firstname'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="lastname">Nachname</label>
        <input type="text" class="form-control <?php if (isset($this->errors['lastname'])) echo 'is-invalid'; ?>" name="lastname" id="lastname" value="<?= $this->data['lastname'] ?? ''; ?>" required>
        <?php
        if (isset($this->errors['lastname'])) {
          echo '<div class="invalid-feedback">' . $this->errors['lastname'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="email">E-Mail</label>
        <input type="text" class="form-control <?php if (isset($this->errors['email'])) echo 'is-invalid'; ?>" name="email" id="email" value="<?= $this->data['email'] ?? ''; ?>" required>
        <?php
        if (isset($this->errors['email'])) {
          echo '<div class="invalid-feedback">' . $this->errors['email'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="email">Passwort</label>
        <input type="password" class="form-control <?php if (isset($this->errors['password'])) echo 'is-invalid'; ?>" name="password" id="password" value="<?= $this->data['password'] ?? ''; ?>" required>
        <?php
        if (isset($this->errors['password'])) {
          echo '<div class="invalid-feedback">' . $this->errors['password'] . '</div>';
        }
        ?>
      </div>
      <div class="form-group mb-2">
        <label for="team_id">Team</label>
        <select name="team_id" id="team_id" class="form-control form-select" required>
          <option class="selected" value="<?= $this->data['team_id'] ?? ''; ?>">Choose Team</option>
          <?php
          foreach ($this->teams as $team) :
          ?>
            <option value="<?= $team->id; ?>"><?= $team->title; ?></option>
          <?php
          endforeach
          ?>
        </select>
      </div>
      <button type="submit" class="btn btn-dark">speichern</button>
    </form>
  </div>
</div>
<?php
include $this->path . '/layouts/footer.php';
?>