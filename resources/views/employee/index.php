<?php
  include $this->path.'/layouts/header.php';
?>
     <div id="wrapper" class="container-fluid">
        <h1>Employees</h1>
        <div class="button"><a href="/employee/create" class="btn btn-outline-secondary">New Employee</a></div>

        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Vorname</th>
                <th scope="col">Nachname</th>
                <th scope="col">E-Mail</th>
                <th scope="col">Abteilung</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
                  foreach($this->employees as $employee) :
              ?>
                <tr>
                  <th scope="row"><?= $employee->id; ?></th>
                  <td><?= $employee->firstname; ?></td>
                  <td><?= $employee->lastname; ?></td>
                  <td><?= $employee->email; ?></td>
                  <td><?= $employee->team_id; ?></td>
                  <td><a href="/employee/edit?id=<?= $employee->id; ?>"
                   class="btn btn-outline-dark fa fa-edit"></a></td>
                  <td scope="col"><form action="/employee" method="post" 
                  class="delete" data-title="<?= $employee->firstname; ?>" 
                  data-body="Should the Employee <strong><?= $employee->firstname; ?></strong> be deleted?">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="<?= $employee->id;?>">
                    <button type="submit" class="btn btn-outline-danger fa fa-trash" 
                    data-bs-toggle="modal" data-bs-target="#deleteModal"></button>
                </form></td>
                </tr>
              <?php
                endforeach;
              ?>
            </tbody>
          </table>
    </div>
<?php
  include $this->path.'/layouts/footer.php';
?>