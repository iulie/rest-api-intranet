<?php

return [
    'mysql' =>[
        'driver'=>'mysql',
        'host'=>env('DB_HOST'),
        'dbname' => env('DB_NAME'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'port' => env('DB_PORT',3306),     
    ],
    'mysql2' =>[
        'driver'=>'mysql',
        'host'=>env('DB_HOST','127.0.0.1'),
        'dbname' => env('DB_NAME'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'port' => env('DB_PORT',3306),     
    ],
    'sqlite' =>[
        'driver'=>'sqlite',
        'path'=>'database'
    ]
];