<?php

namespace App;

session_start();
use App\Core\Routes;

class Bootstrap{
    
    private static $instanz = NULL;
    public static $route;
    protected $data = [];

    private function __construct(){}

    public static function init(){
        if( is_null(self::$instanz) ){
            self::$instanz = new Bootstrap;
            self::$route = new Routes;
        }
        return self::$instanz;
    }

    public function __set($key,$value){
        $this->data[$key] = $value;
    }

    public function __get($key){
        return $this->data[$key] ?? ' Route ';
    }

    public static function run(){
        self::$route->initController();
    }
}