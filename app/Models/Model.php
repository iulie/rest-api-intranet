<?php

namespace App\Models;

class Model{
    
    protected $db;
    protected $select = '*';
    protected $order_by = '';
    protected $where = '';
    protected $params = [];


    public function __construct(){

        if( !isset($this->connection) ){
            $this->connection = env('DB_CONNECTION');
        }
        //1. Index driver aus config/database.php auslesen
        $account = config('database.'.$this->connection);
        $db = '\App\Core\\'.ucfirst($account['driver']);
        //2. Instanz von der DB Klasse erstellen
        $this->db = new $db($account);

        if( !isset($this->table) ){
            $table = get_class($this);
            $this->table = strtolower(str_replace(__NAMESPACE__.'\\','',$table).'s');
        }
        $this->db->table($this->table);
    }

    public function get(){
        $sql = 'SELECT '.$this->select.' FROM '.$this->table;
        if( $this->where ){
            $sql .= ' WHERE '.$this->where;
        }
        if( $this->order_by ){
            $sql .= ' ORDER BY '.$this->order_by;
        }
        $result = $this->db->get($sql,$this->params);
        $this->reset();
        return $result;
    }

    protected  function reset(){
        $this->select = '*';
        $this->order_by = '';
        $this->where = '';
        $this->params = [];
    }

    //spread Operator
    public function select(...$select){
        $this->select = implode(',',$select);
        return $this;
    }

    public function orderBy(string $column, string $sort='asc'){
        $this->order_by .= ($this->order_by) ? ','.$column.' '.$sort : $column.' '.$sort;
        return $this;
    }

    public function where(string $column, string $condition, $value)
    {
        $this->where .= ($this->where) ? ' AND ' . $column . ' ' . $condition . ' ?' : $column . ' ' . $condition . ' ?';
        $this->params[] = $value;
        return $this;
    }

    public function orWhere(string $column, string $condition, $value){
        $this->where .= ($this->where) ? ' OR '.$column.$condition.':'.$column : $column.$condition.':'.$column;
        $this->params[$column] =$value;
        return $this;
    }

    public function find($id,$column = 'id'){
        $this->where($column,'=',$id);
        $result = $this->get();
        return empty($result ) ? false : $result[0];
    }
    
    public function create(array $data){
        return $this->db->create($data);
    }
    
    /**
     * Datensatz ändern
     *
     * @param   array  $data    Assoziatives Array -> Keys entsprechen den Tabellen Spalten der Datenbank
     * @param   mix $id      ID von dem Datensatz der geändert werden soll
     * @param   string $column  Spaltenname der ID
     *
     * @return  bool         geändert oder nicht geändert
     */
    public function update(array $data,$id,$column='id'){
        return $this->db->update($data,$id,$column='id');
    }

    public function delete($id,$column='id'){
        return $this->db->delete($id,$column='id');
    }

    public function belongsTo($relatedModel, $foreignKey, $ownerKey = 'id')
    {
        $relatedInstance = new $relatedModel();
        $table = $relatedInstance->getTable();
        return $this->where($foreignKey, '=', $this->$ownerKey)->getRelatedModel($relatedModel, $table);
    }

    public function hasMany($relatedModel, $foreignKey, $ownerKey = 'id')
    {
        $relatedInstance = new $relatedModel();
        $table = $relatedInstance->getTable();
        return $relatedInstance->where($foreignKey, '=', $this->$ownerKey)->getRelatedModel($relatedModel, $table);
    }

    private function getRelatedModel($relatedModel, $table)
    {
        $relatedInstance = new $relatedModel();
        $relatedInstance->table($table);
        return $relatedInstance;
    }

}