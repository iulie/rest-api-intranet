<?php
namespace App\Models;

use App\Models\Employee;

class Team extends Model
{

    public function employees()
    {
       return $this->hasMany(Employee::class, 'team_id');
    }
}