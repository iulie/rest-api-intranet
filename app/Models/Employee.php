<?php
namespace App\Models;

use Team;


class Employee extends Model
{
    
    public function team() {
     return $this->belongsTo(Team::class, 'team_id');
    }
}