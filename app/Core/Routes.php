<?php

namespace App\Core;

use App\Controllers\Auth\Auth;


class Routes
{

    public array $get, $post, $put, $delete = [];

    private function addRoute(array $uri): array
    {
        return [
            'controller' => $uri[0],
            'method' => $uri[1]
        ];
    }

    private function addFunction($action): array
    {
        return [
            'function' => $action
        ];
    }

    public function get(string $uri, $action): void
    {
        if (is_array($action) && count($action) >= 2) {
            $this->get[$uri] = $this->addRoute($action);
        } else if (is_callable($action)) {
            $this->get[$uri] = $this->addFunction($action);
        }
    }

    public function post(string $uri, $action): void
    {
        if (is_array($action)  && count($action) >= 2) {
            $this->post[$uri] = $this->addRoute($action);
        } else if (is_callable($action)) {
            $this->post[$uri] = $this->addFunction($action);
        }
    }

    public function put(string $uri, $action): void
    {
        if (is_array($action) && count($action) >= 2) {
            $this->put[$uri] = $this->addRoute($action);
        } else if (is_callable($action)) {
            $this->put[$uri] = $this->addFunction($action);
        }
    }

    public function delete(string $uri, $action): void
    {
        if (is_array($action) && count($action) >= 2) {
            $this->delete[$uri] = $this->addRoute($action);
        } else if (is_callable($action)) {
            $this->delete[$uri] = $this->addFunction($action);
        }
    }


    public function initController()
    {

        if ("/auth/login" !== $this->getUri()) {
            if (!Auth::isUserLoggedIn()) {
                header("Location: /auth/login");
            }
        }
        $method = $this->getHttpMethod();
        $uri = $this->getUri();


        $action = $this->$method[$uri] ?? false;
        if (!$action) {
            http_response_code(404);
            view('errors.404');
            return;
        }
        if (isset($action['function'])) {
            $function = $action['function'];
            return $function();
        }
        $controller = $action['controller'];
        $method = $action['method'];
        if (!method_exists($controller, $method)) {
            if (config('app.debug')) {
                if (!class_exists($controller)) {
                    $error = 'Der Controller ' . $controller . ' existiert nicht';
                } else {
                    $error = 'Die Methode ' . $method . ' im Controller ' . $controller . ' existiert nicht';
                }
                dd($error);
            }
            http_response_code(500);
            view('errors.500');
            return;
        }
        $controller = new $controller;
        $controller->$method();
        return;
    }

    protected function getHttpMethod(): string
    {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        if ($method == 'post' && isset($_POST['_method'])) {
            $method = strtolower($_POST['_method']);
        }
        return $method;
    }

    protected function getUri(): array|string|null
    {
        $uri = $_SERVER['REQUEST_URI'];
        //1. Falls ein / am Ende steht -> entfernen AUSNAHME: Falls REQUEST_URI / liefert
        if ($uri == '/' || empty($uri)) {
            return '/';
        }
        //2. Falls QUERY_STRING z.B. /abteilung/edit/?id=10 -> ?id=10 entfernen
        if (!empty($_SERVER['QUERY_STRING'])) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        //3. Falls in REQUEST_URI nichts übergeben wird -> /
        return preg_replace('/(\/)$/', '', $uri);
    }
}
