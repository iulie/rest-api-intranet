<?php

namespace App\Core;

use PDO;
use PDOException;

class Mysql
{

    protected $pdo;
    protected $errors = [];
    protected $table = '';

    public function __construct(array $account)
    {
        $this->con($account);
    }

    protected function con(array $account)
    {
        try {
            $dsn = 'mysql:host=' . $account['host'] . ';dbname=' . $account['dbname'] . ';port=' . $account['port'];
            $this->pdo = new PDO($dsn,$account['username'],$account['password']);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            $this->errors['con'] = $e->getMessage();
        }
    }

    public function getError(string $key)
    {
        return $this->errors[$key] ?? '';
    }

    public function query(string $statement)
    {
        unset($this->errors['query']);
        try {
            return $this->pdo->query($statement);
        } catch (PDOException $e) {
            $this->errors['query'] = $e->getMessage();
        }
    }

    public function table(string $table)
    {
        $this->table = $table;
        return $this;
    }

    public function create(array $data)
    {
        unset($this->errors['create']);

        $dataKeys = array_keys($data);
        $columns = implode(',', $dataKeys);
        $values = ':' . implode(',:', $dataKeys);

        $sql = 'INSERT INTO ' . $this->table . ' (' . $columns . ') VALUES(' . $values . ');';
        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($data);
            return $this->pdo->lastInsertId();
        } catch (PDOException $e) {
            $this->errors['create'] = $e->getMessage();
        }
        return false;
    }

    public function all(array $columns = ['*'])
    {
        unset($this->errors['all']);
        $columns = implode(',', $columns);
        $sql = 'SELECT ' . $columns . ' FROM ' . $this->table;
        try {
            $records = $this->query($sql);
            return $records->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $e) {
            $this->errors['all'] = $e->getMessage();
        }
        return false;
    }

    public function get(string $sql, array $params = [])
    {
        unset($this->errors['get']);
        try {
            $records = $this->pdo->prepare($sql);
            $records->execute($params);
            return $records->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $e) {
            $this->errors['get'] = $e->getMessage();
        }
        return false;
    }

    public function update(array $data, $id, $where_column = 'id')
    {
        unset($this->errors['update']);
        try {
            $arrayColumns = array_keys($data);
            $set = '';
            foreach ($arrayColumns as $column) {
                $set .= ($set == '') ? $column . '=:' . $column : ',' . $column . '=:' . $column;
            }

            $sql = 'UPDATE ' . $this->table . ' SET ' . $set . ' WHERE ' . $where_column . '=:' . $where_column;

            $statement = $this->pdo->prepare($sql);
            $data[$where_column] = $id;
            $statement->execute($data);
            return true;
        } catch (PDOException $e) {
            $this->errors['update'] = $e->getMessage();
        }
        return false;
    }

    public function delete($id, $column = 'id')
    {
        unset($this->errors['delete']);
        try {
            $sql = 'DELETE FROM ' . $this->table . ' WHERE ' . $column . '=:' . $column;
            $statement = $this->pdo->prepare($sql);
            $statement->execute([$column => $id]);
            return true;
        } catch (PDOException $e) {
            $this->errors['delete'] = $e->getMessage();
        }
        return false;
    }
}
