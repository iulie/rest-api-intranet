<?php

namespace App\Core;

class Validation{
    private $data;
    private $errors = [];

    protected function validate(array $rules, array $data){
        $this->data = $data;
        foreach( $rules as $key=>$value){
            $rulesData = explode('|',$value);
            foreach( $rulesData as $rule){
                $ruleMethods = explode(':',$rule);
                switch($ruleMethods[0]){
                    case 'required' : $is_valid = $this->required($key); break;
                    case 'min' : $is_valid = $this->min($key,$ruleMethods[1]); break;
                    case 'max' : $is_valid = $this->max($key,$ruleMethods[1]); break;
                }
                if( !$is_valid){
                    break;
                }
            }     
        }
        return ( empty($this->errors) ) ? true : false;
    }

    private function required(string $key){
        if( !isset($this->data[$key]) || trim($this->data[$key]) == '' ){
            $this->errors[$key] = ucfirst($key).' ist ein Pflichtfeld';
            return false;
        }
        return true;
    }

    private function min(string $key, int $len){
        if( mb_strlen( trim($this->data[$key]) ) < $len ){
            $this->errors[$key] = ucfirst($key).' muss min. '.$len.' Zeichen lang sein';
            return false;
        }
        return true;
    }

    private function max(string $key, int $len){
        if( mb_strlen( trim($this->data[$key]) ) > $len ){
            $this->errors[$key] = ucfirst($key).' darf max. '.$len.' Zeichen lang sein';
            return false;
        }
        return true;
    }

    protected function getErrors(){
        return (empty($this->errors) ) ? false : $this->errors;
    }

    protected function getData(){
        return $this->data;
    }
}