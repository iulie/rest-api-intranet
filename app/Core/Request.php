<?php

namespace App\Core;

class Request extends Validation
{

    public function getData()
    {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        switch ($method) {
            case 'get':
                return $_GET;
            case 'post':
                return $this->post();
            case 'put':
                return $this->put();
            case 'delete':
                return $this->delete();
        }
    }

    private function post()
    {
        $requestData = [];

        if (!empty($_POST)) {
            $requestData = $_POST;
        }

        // Datei-Upload verarbeiten
        if (!empty($_FILES)) {
            foreach ($_FILES as $key => $file) {
                if (is_uploaded_file($file['tmp_name'])) {
                    $requestData[$key] = $file['tmp_name'];
                }
            }
        }
        return $requestData;
    }

    private function put()
    {
        $data = file_get_contents('php://input');
        return empty($data) ? [] : json_decode($data, true);
    }

    private function delete()
    {
        $data = file_get_contents('php://input');
        return empty($data) ? [] : json_decode($data, true);
    }
}
