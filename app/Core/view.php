<?php

namespace App\Core;

class View
{
    protected $path;
    protected $template;
    protected $_ = [];
    static protected $s_ = [];

    public function __construct($path, $template, array $data)
    {
        $this->path = $path;
        $this->template = $template;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function __set(string $key, $value)
    {
        $this->_[$key] = $value;
    }

    public function __get($key)
    {
        return $this->_[$key] ?? '';
    }

    static public function sassign(string $key, mixed $value): mixed
    {
        self::$s_[$key] = $value;
        return $value;
    }

    public function get(string $key, mixed $default = ''): mixed
    {
        return self::$s_[$key] ?? $this->_[$key] ?? $default;
    }

    public function load(): string
    {
        $tpl = $this->path . $this->template;
        if (file_exists($tpl)) {
            // Output wird in ein Buffer gespeichert, d.h. nicht ausgegeben
            ob_start();

            include $tpl;

            $output = ob_get_contents();
            ob_end_clean();

            return $output;
        } else {
            error_log($tpl . ' not found!');
            return '';
        }
    }

    public function include(string $template): string
    {
        return $this->load($template);
    }
}
