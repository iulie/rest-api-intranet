<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Project;
use App\Models\Employee;

class ProjectController extends Request
{

    public function index()
    {
        $projects = (new Project)->select('id', 'name', 'description', 'image')->orderBy('id')->get();
        return view('project.index', compact('projects'));
    }

    public function show()
    {
        if (!isset($_GET['id'])) {
            http_response_code(404);
            return view('errors.404');
        }
        $project = (new Project)->find($_GET['id']);
        if (!$project) {
            http_response_code(404);
            return view('errors.404');
        }

        return view('project.show', compact('project'));
    }

    public function create()
    {
        return view('project.create');
    }

    public function store()
    {
        $data = $this->getData();

        $rules = [
            'name' => 'required|min:2|max:100',
            'description' => 'required|min:5',
            'image' => 'required'
        ];
        $valid = $this->validate($rules, $data);

        if (!$valid) {
            $errors = $this->getErrors();
            return view('project.create', compact('data', 'errors'));
        }
        if (isset($data['image'])) {
            $file = $_FILES['image'];
            $fileName = time() . '_' . $file['name'];
            $fileTmpPath = $file['tmp_name'];
            $destinationPath = 'uploads/' . $fileName;
            move_uploaded_file($fileTmpPath, $destinationPath);
            $data['image'] = $fileName;
        }

        $project = (new Project)->create($data);
        header('LOCATION:/project');
        exit;
    }

    public function edit()
    {
        if (!isset($_GET['id'])) {
            http_response_code(404);
            return view('errors.404');
        }

        $project = (new Project)->find($_GET['id']);

        if (empty($project)) {
            http_response_code(404);
            return view('errors.404');
        }
        $data = ['id' => $project->id, 'name' => $project->name, 'description' => $project->description, 'image' => $project->image];

        return view('project.edit', compact('data'));
    }


    public function update()
    {

        $data = $this->getData();
        if (!isset($data['id'])) {
            http_response_code(404);
            return view('errors.404');
        }
        $project = (new Project)->find($data['id']);
        if (empty($project)) {
            http_response_code(404);
            return view('errors.404');
        }
        $rules = [
            'name' => 'required|min:2|max:100',
            'description' => 'required|min:5',
            'image' => 'required'
        ];

        $valid = $this->validate($rules, $data);

        if (!$valid) {
            $errors = $this->getErrors();
            return view('project.edit', compact('data', 'errors'));
        }

        unset($data['_method']);
        $id = $data['id'];
        unset($data['id']);
        $result = (new Project)->update($data, $id);
        header('LOCATION:/project');
        return;
    }

    public function destroy()
    {
        $data = $this->getData();
        $error = '';
        if (!isset($data['id'])) {
            http_response_code(404);
            $error = 404;
        }
        $project = (new Project)->find($data['id']);
        if (empty($project)) {
            http_response_code(404);
            $error = 404;
        }
        if (!$error) {
            $project = (new Project)->delete($data['id']);
        }
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            header('Content-Type: application/json');
            if ($error) {
                echo '{"status":' . $error . '}';
            }
            echo '{"status":200,"msg":"Abteilung wurde gelöscht"}';
            return;
        }
        if ($error) {
            return view('errors.' . $error);
        }
        header('LOCATION:/project');
        return;
    }
}
