<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Team;
use App\Models\Employee;

class EmployeeController extends Request
{

    public function index()
    {
        $employees = (new Employee)->select('id', 'team_id', 'firstname', 'lastname', 'email')->orderBy('id')->get();
        return view('employee.index', compact('employees'));
    }

    public function create()
    {
        $teams = (new Team)->select('id', 'title', 'body')->orderBy('title')->get();
        return view('employee.create', compact('teams'));
    }


    public function store()
    {
        $data = $this->getData();
        $valid = $this->validate($this->getRules(), $data);

        if (!$valid) {
            $errors = $this->getErrors();
            return view('employee.create', compact('data', 'errors'));
        }
        $data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);
        (new Employee)->create($data);

        header('LOCATION:/employee');
        exit;
    }

    public function edit()
    {
        if (!isset($_GET['id'])) {
            http_response_code(404);
            return view('errors.404');
        }

        $employee = (new Employee)->find($_GET['id']);

        if (empty($employee)) {
            http_response_code(404);
            return view('errors.404');
        }
        $data = [
            'id' => $employee->id,
            'firstname' => $employee->firstname,
            'lastname' => $employee->lastname,
            'email' => $employee->email,
            'password' => $employee->password,
            'team_id' => $employee->team_id
        ];

        $teams = (new Team)->select('id', 'title', 'body')->orderBy('title')->get();

        return view('employee.edit', compact('data','teams'));
    }


    public function update()
    {
        $data = $this->getData();
        if (!isset($data['id'])) {
            http_response_code(404);
            return view('errors.404');
        }
        $employee = (new Employee)->find($data['id']);
        if (empty($employee)) {
            http_response_code(404);
            return view('errors.404');
        }

        $valid = $this->validate($this->getRules(), $data);
        if (!$valid) {
            $errors = $this->getErrors();
            return view('team.edit', compact('data', 'errors'));
        }
        unset($data['_method']);
        $id = $data['id'];
        (new Employee)->update($data, $id);
        header('LOCATION:/employee');
        exit;
    }

    public function destroy()
    {
        $data = $this->getData();
        $error = '';
        if (!isset($data['id'])) {
            http_response_code(404);
            $error = 404;
        }
        $employee = (new Employee)->find($data['id']);
        if (empty($employee)) {
            http_response_code(404);
            $error = 404;
        }
        if (!$error) {
            (new Employee)->delete($data['id']);
        }
        

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            header('Content-Type: application/json');
            if ($error) {
                echo '{"status":' . $error . '}';
            }
            echo '{"status":200,"msg":"Abteilung wurde gelöscht"}';
            return;
        }

        if ($error) {
            return view('errors.' . $error);
        }
        header('LOCATION:/employee');
        return;
    }
    
    private function getRules(): array
    {
        return [
            'firstname' => 'required|min:2|max:100',
            'lastname' => 'required|min:2|max:100',
            'email' => 'required|min:2|max:100',
            'password' => 'required|min:8|max:100',
            'team_id' => 'required'
        ];
    }
}
