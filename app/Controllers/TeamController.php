<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Team;
use App\Models\Employee;

class TeamController extends Request {

    public function index(){
        $teams = (new Team)->select('id','title','body')->orderBy('id')->get();
        return view('team.index', compact('teams'));
    }

    public function show(){
        if( !isset($_GET['id']) ){
            http_response_code(404);
            return view('errors.404');
        }
        
        $team = (new Team)->find($_GET['id']);
        if( !$team){
            http_response_code(404);
            return view('errors.404');
        }

        $employees = (new Employee)->select('id', 'team_id', 'firstname', 'lastname', 'email')
                                    ->where('team_id', '=', $team->id)->get();
        return view('team.show',compact('team', 'employees'));
    }

    public function create(){
        return view('team.create');
    }

    public function store(){
        $data = $this->getData();
        
        $rules = [
            'title' => 'required|min:2|max:100',
            'body' =>'required|min:5'
        ];
        //Daten validieren
        $valid = $this->validate($rules,$data);
            //Falls Daten nicht OK -> Formular mit Fehlermeldungen aufrufen
        if( !$valid ){
            $errors = $this->getErrors();

            /* $data = [
                    'data' => $data,
                    'errors' =>$errors 
                ]
                dd( compact('data','errors') );
            */
            return view('team.create',compact('data','errors'));
        }
        
        $team  = (new Team)->create($data);
            //Falls Daten OK -> Daten in DB speichern und alle Abteilungen anzeigen
        header('LOCATION:/team');
        exit;
    }

    public function edit(){
         //1. Prüfen ob id vorhanden
            //Falls nicht -> view 404
        if( !isset($_GET['id']) ){
            http_response_code(404);
            return view('errors.404');
        }
        
        //2. Datenstatz auslesen
        $team= (new Team)->find($_GET['id']);

        //Falls nicht vorhanden -> view 404
        if( empty($team) ){
            http_response_code(404);
            return view('errors.404');
        }
        $data = ['id'=>$team->id,'title'=>$team->title,'body'=>$team->body];
      
        //DS vorhanden -> neue View show.php
          //Falls vorhanden -> neuer view edit.php und Datensatz an view übergeben
        return view('team.edit',compact('data'));    
    }


    public function update(){
        //1. Daten einlesen
        $data = $this->getData();
       //dd($data);
        //2. Prüfen ob id existiert
            //Falls nicht -> 404
            if( !isset($data['id']) ){
                http_response_code(404);
                return view('errors.404');
            }
            //3. DS aus DB auslesen
                $team= (new Team)->find($data['id']);
                //Falls kein DS -> 404
                if( empty($team) ){
                    http_response_code(404);
                    return view('errors.404');
                }
                //Falls DS existiert
                //4. Daten validieren
                $rules = [
                    'title' => 'required|min:2|max:100',
                    'body' =>'required|min:5'
                ];
                //Daten validieren
                $valid = $this->validate($rules,$data);
                    //Falls Daten nicht korrekt -> Formular ausgeben inkl. Fehlermeldungen
                    if( !$valid ){
                        $errors = $this->getErrors();
                        return view('team.edit',compact('data','errors'));
                    }
                    //Falls Daten korrekt -> DS ändern
                    unset($data['_method']);
                    $id = $data['id'];
                    unset($data['id']);
                    $result = (new Team)->update( $data,$id);
                        //5. redirect to all teams
                        header('LOCATION:/team');
                        return;
    }

    public function destroy(){
        //1. Daten einlesen
        $data = $this->getData();
        $error = '';
        //dd($_SERVER);
        //$_SERVER['HTTP_X_REQUESTED_WITH']
        //dd($data);
        //2. überprüfen ob ID vorhanden
            //Falls nicht -> 404
            if( !isset($data['id']) ){
                http_response_code(404);
                $error=404;
            }
            //3. Überprüfen ob DS vorhanden
                $team= (new Team)->find($data['id']);
                //Falls nicht -> 404
                if( empty($team) ){
                    http_response_code(404);
                    $error = 404;
                }
                if( !$error ){
                    //Falls ja -> Datensatz löschen
                    $team = (new Team)->delete($data['id']);
                }
                
                //AJAX
                if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){
                    header('Content-Type: application/json');
                    if( $error){
                        echo '{"status":'.$error.'}';
                    }
                    echo '{"status":200,"msg":"Abteilung wurde gelöscht"}';
                    return;
                }
                
                //HTML
                if( $error ){
                    return view('errors.'.$error);
                }
                //4. Umleiten auf Seite alle Abteilungen
                header('LOCATION:/team');
                return;
    }
}