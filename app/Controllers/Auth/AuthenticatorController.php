<?php
namespace App\Controllers\Auth;

use App\Core\Request;
use App\Models\Employee;

class AuthenticatorController extends Request
{
    public function index() {
        return view('auth.login');
    }

    public function store() {
        $data = $this->getData();

        $rules = [
            'email' => 'required',
            'password' =>'required'
        ];

        $valid = $this->validate($rules,$data);

        if( !$valid ) {
            $errors = $this->getErrors();
            return view('auth.login',compact('data','errors'));
        }
        $user = (new Employee())->select("id", "email", "password")->where("email", "=", $data["email"])->get();
        if ($user[0]->email === $data["email"] && password_verify($data["password"], $user[0]->password)) {
            session_regenerate_id();
            $_SESSION["loggedIn"] = true;
            $_SESSION["userId"] = $user[0]->id;
            $_SESSION["email"] = $user[0]->email;
            header('LOCATION:/');
        }
        else {
            $_SESSION["error_login"] = "Email or Password is not correct!";
            header("Location: /auth/login");
        }
        exit();

    }

    public function logout() {
        session_destroy();
        header('LOCATION: /auth/login');
    }
}