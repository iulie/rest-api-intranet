<?php

namespace App\Controllers\Auth;
class Auth
{
    public static function isUserLoggedIn(): bool
    {
        return isset($_SESSION["userId"]) && isset($_SESSION["loggedIn"]);
    }

    public static function getCurrentUserId(): string|null
    {
        return $_SESSION["userId"] ?? null;
    }
}